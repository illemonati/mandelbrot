// use raster::Color;
use num::complex::Complex;
use palette::{Hsva, rgb::Rgba, RgbHue};
use mandelbrot::mandelbrot_iterations;
// use raster::Image;
use image::{ImageBuffer, RgbImage, Rgb};
use rayon::prelude::*;

const MAX_ITERATIONS: usize = 80;

const RE_START: isize = -2;
const RE_END: isize = 1;
const IM_START: isize = -1;
const IM_END: isize = 1;

const WIDTH: usize = 60000;
const HEIGHT: usize = 40000;

mod viewer;

fn main() {
    // generate_and_save_image();
    viewer::run_viewer();
}

fn generate_and_save_image() {
    // let mut image = Image::blank(WIDTH as i32, HEIGHT as i32);
    let mut image: RgbImage = ImageBuffer::new(WIDTH as u32, HEIGHT as u32);
    let image_data: Vec<Vec<image::Rgb<u8>>> = (0..WIDTH).into_par_iter().map(|x| {
        (0..HEIGHT).into_par_iter().map(move |y| {
            let c = Complex::new(
                RE_START as f64 + (x as f64 / WIDTH as f64) * (RE_END - RE_START) as f64,
                IM_START as f64 + (y as f64 / HEIGHT as f64) * (IM_END - IM_START) as f64,
            );
            let iterations = mandelbrot_iterations(c, MAX_ITERATIONS);
            let hue = (iterations as f64 / MAX_ITERATIONS as f64) * 360f64;
            let sat = 1f64;
            let val = if iterations < MAX_ITERATIONS { 1f64 } else { 0f64 };
            let color = Hsva::new(RgbHue::from_degrees(hue as f32), sat as f32, val as f32, 1.0);
            let rgb_color: Rgba = Rgba::from(color);
            Rgb([(rgb_color.red * 255.0) as u8, (rgb_color.green * 255.0) as u8, (rgb_color.blue * 255.0) as u8])
        }).collect()
    }).collect();
    for x in 0..WIDTH {
        for y in 0..HEIGHT {
            image.put_pixel(x as u32, y as u32, image_data[x][y])
        }
    }
    image.save("out/mandelbrot.png").expect("save error");
}
