use num::complex::Complex;
use num::Zero;


pub struct MandelBrot {
    pub iterations: usize,
    max_iterations: Option<usize>,
    z: Complex<f64>,
    c: Complex<f64>,
}

impl MandelBrot {
    pub fn new(c: Complex<f64>, max_iterations: Option<usize>) -> Self {
        Self {
            iterations: 0,
            max_iterations,
            z: Complex::zero(),
            c
        }
    }
}

impl Iterator for MandelBrot {
    type Item = Complex<f64>;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(max_iterations) = self.max_iterations {
            if self.iterations >= max_iterations {
                return None;
            }
        }
        self.z = self.z * self.z + self.c;
        self.iterations += 1;
        Some(self.z)
    }
}