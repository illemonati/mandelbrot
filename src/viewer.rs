use pixels::{Error, Pixels, SurfaceTexture};
use winit::dpi::{LogicalPosition, LogicalSize, PhysicalSize};
use winit::event::{Event, VirtualKeyCode};
use winit::event_loop::{ControlFlow, EventLoop};
use winit_input_helper::WinitInputHelper;
use num::Complex;
use mandelbrot::mandelbrot_iterations;
use palette::{Hsva, rgb::Rgba, RgbHue};
use rayon::iter::IntoParallelIterator;
use rayon::iter::ParallelIterator;
use std::sync::Mutex;


const SCREEN_WIDTH: u32 = 1200;
const SCREEN_HEIGHT: u32 = 800;
const ITERATIONS: usize = 20;

fn create_window(
    title: &str,
    event_loop: &EventLoop<()>,
) -> (winit::window::Window, u32, u32, f64) {
    // Create a hidden window so we can estimate a good default window size
    let window = winit::window::WindowBuilder::new()
        .with_visible(false)
        .with_title(title)
        .build(&event_loop)
        .unwrap();
    let hidpi_factor = window.scale_factor();

    // Get dimensions
    let width = SCREEN_WIDTH as f64;
    let height = SCREEN_HEIGHT as f64;
    let (monitor_width, monitor_height) = {
        let size = window.current_monitor().size();
        (
            size.width as f64 / hidpi_factor,
            size.height as f64 / hidpi_factor,
        )
    };
    let scale = (monitor_height / height * 2.0 / 3.0).round();

    // Resize, center, and display the window
    let min_size: winit::dpi::LogicalSize<f64> =
        PhysicalSize::new(width, height).to_logical(hidpi_factor);
    let default_size = LogicalSize::new(width * scale, height * scale);
    let center = LogicalPosition::new(
        (monitor_width - width * scale) / 2.0,
        (monitor_height - height * scale) / 2.0,
    );
    window.set_inner_size(default_size);
    window.set_min_inner_size(Some(min_size));
    window.set_outer_position(center);
    window.set_visible(true);

    let size = default_size.to_physical::<f64>(hidpi_factor);
    (
        window,
        size.width.round() as u32,
        size.height.round() as u32,
        hidpi_factor,
    )
}


pub fn run_viewer() -> Result<(), Error> {
    let event_loop = EventLoop::new();
    let mut input = WinitInputHelper::new();
    let (window, p_width, p_height, mut _hidpi_factor) =
        create_window("Mandelbrot Viewer", &event_loop);
    let surface_texture = SurfaceTexture::new(p_width, p_height, &window);
    let mut pixels = Pixels::new(SCREEN_WIDTH, SCREEN_HEIGHT, surface_texture)?;

    // window of the function
    let mut re_start: f64 = -2.0;
    let mut re_end: f64 = 1.0;
    let mut im_start: f64 = -1.0;
    let mut im_end: f64 = 1.0;
    let mut image_drawn = false;
    let mut zoom_level: usize = 1;


    event_loop.run(move |event, _, control_flow| {

        if let Event::RedrawRequested(_) = event {
            println!("redraw requested, zoom level: {}", zoom_level);
            let frame = Mutex::new(pixels.get_frame());

            (0..SCREEN_WIDTH).into_par_iter().for_each(|x| {
                (0..SCREEN_HEIGHT).into_par_iter().for_each(|y|{
                    // println!("{} {}", re_start, re_end);
                    let c = Complex::new(
                        re_start as f64 + (x as f64 / SCREEN_WIDTH as f64) * (re_end - re_start),
                        im_start as f64 + (y as f64 / SCREEN_HEIGHT as f64) * (im_end - im_start),
                    );
                    let iterations = mandelbrot_iterations(c, zoom_level * ITERATIONS);
                    let hue = (iterations as f64 / (zoom_level * ITERATIONS) as f64) * 360f64;
                    let sat = 1f64;
                    let val = if iterations < (zoom_level * ITERATIONS) { 1f64 } else { 0f64 };
                    let color = Hsva::new(RgbHue::from_degrees(hue as f32), sat as f32, val as f32, 1f32);
                    let rgb_color: Rgba = Rgba::from(color);
                    let pixel = [(rgb_color.red * 255f32) as u8, (rgb_color.green * 255f32) as u8, (rgb_color.blue * 255f32) as u8, 255u8];
                    let index: usize = (4*(SCREEN_WIDTH*y+x)) as usize;
                    frame.lock().unwrap()[index..index+4].copy_from_slice(&pixel);
                });
            });

            if pixels
                .render()
                .map_err(|e| eprintln!("pixels.render() failed: {}", e))
                .is_err()
            {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }

        if input.update(&event) {
            if input.key_pressed(VirtualKeyCode::Escape) || input.quit() {
                *control_flow = ControlFlow::Exit;
                return;
            }
            if input.mouse_pressed(0) {
                let (mouse_x, mouse_y) = input.mouse().unwrap();
                println!("click at x: {}, y: {}", mouse_x, mouse_y);
                let (pixel_x, pixel_y) = pixels.window_pos_to_pixel((mouse_x, mouse_y)).unwrap();
                println!("pixel location x: {}, y: {}", pixel_x, pixel_y);
                let x_diameter = (re_end - re_start);
                let y_diameter = (im_end - im_start);
                let center_x = re_start as f64 + (pixel_x as f64 / SCREEN_WIDTH as f64) * x_diameter;
                let center_y = im_start as f64 + (pixel_y as f64 / SCREEN_HEIGHT as f64) * y_diameter;
                let new_x_radius = x_diameter/4.0;
                let new_y_radius = y_diameter/4.0;
                re_start = center_x - new_x_radius;
                re_end = center_x + new_x_radius;
                im_start = center_y - new_y_radius;
                im_end = center_y + new_y_radius;
                zoom_level += 1;
                window.request_redraw();
            }
            if input.mouse_pressed(1) {
                let (mouse_x, mouse_y) = input.mouse().unwrap();
                println!("click at x: {}, y: {}", mouse_x, mouse_y);
                let (pixel_x, pixel_y) = pixels.window_pos_to_pixel((mouse_x, mouse_y)).unwrap_or_else(|pos| pixels.clamp_pixel_pos(pos));;
                println!("pixel location x: {}, y: {}", pixel_x, pixel_y);
                let x_diameter = (re_end - re_start);
                let y_diameter = (im_end - im_start);
                let center_x = re_start as f64 + (pixel_x as f64 / SCREEN_WIDTH as f64) * x_diameter;
                let center_y = im_start as f64 + (pixel_y as f64 / SCREEN_HEIGHT as f64) * y_diameter;
                let new_x_radius = x_diameter;
                let new_y_radius = y_diameter;
                re_start = center_x - new_x_radius;
                re_end = center_x + new_x_radius;
                im_start = center_y - new_y_radius;
                im_end = center_y + new_y_radius;
                zoom_level -= 1;
                window.request_redraw();
            }
            if !image_drawn {
                window.request_redraw();
                image_drawn = true;
            }
        }
    });
}
