mod mandelbrot;

use num::complex::Complex;
use mandelbrot::MandelBrot;



pub fn mandelbrot_iterations(c: Complex<f64>, max_iterations: usize) -> usize {
    let mut iter = MandelBrot::new(c, Some(max_iterations));
    loop {
        let next = iter.next();
        if let Some(next) = next {
            if next.norm() <= 2.0 {
                continue;
            }
        }
        break;
    }
    return iter.iterations;
}


